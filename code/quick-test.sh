#!/bin/bash
# SPDX-FileCopyrightText: 2018 German Aerospace Center (DLR)
# SPDX-License-Identifier: MIT


# Exit when any command fails
set -e

# Install required dependencies
pip install -r requirements.txt
echo "Successfully installed required packages"

# Run tests
pytest --disable-warnings
echo "Successfully ran tests"

# Check the code using the flake8 linter
flake8 --max-line-length 120 astronaut_analysis.py
echo "Successfully ran flake8 checks"

# Check that copyright and license information for all files is available
reuse --root ../ lint
echo "Successfully ran reuse checks"
