"""
SPDX-FileCopyrightText: German Aerospace Center (DLR)
SPDX-License-Identifier: MIT


Run basic tests.

"""


import datetime

import pandas as pd
import pytest

import astronaut_analysis


def test_calculate_died_with_age_no_death_date():
    assert astronaut_analysis.calculate_died_with_age({"date_of_death": None}) is None


def test_calculate_died_with_age_no_birthdate():
    row = {
        "date_of_death": datetime.date(1990, 6, 6),
        "birthdate": None,
    }
    assert astronaut_analysis.calculate_died_with_age(row) is None


@pytest.mark.parametrize(
    "birthdate, date_of_death, age",
    [
        (datetime.date(1990, 6, 6), datetime.date(1990, 6, 6), 0),
        (datetime.date(1990, 4, 6), datetime.date(1990, 6, 6), 0),
        (datetime.date(1940, 6, 6), datetime.date(1990, 6, 6), 50),
        (datetime.date(1940, 5, 6), datetime.date(1990, 6, 6), 50),
        (datetime.date(1940, 7, 6), datetime.date(1990, 6, 6), 49),
    ],
)
def test_calculate_died_with_age_valid(birthdate, date_of_death, age):
    row = {"birthdate": birthdate, "date_of_death": date_of_death}
    assert astronaut_analysis.calculate_died_with_age(row) == age


def test_calculate_died_with_age_died_before_born():
    row = {
        "birthdate": datetime.date(1990, 6, 6),
        "date_of_death": datetime.date(1940, 6, 6),
    }
    assert astronaut_analysis.calculate_died_with_age(row) is None


def test_calculate_age_valid():
    birthdate = datetime.date(1900, 1, 1)
    assert birthdate < astronaut_analysis.ASTRONAUT_DATA_RETRIEVAL_DATE
    assert astronaut_analysis.calculate_age(birthdate) > 0


def test_calculate_age_born_after_today():
    birthdate = datetime.date(2200, 1, 1)
    assert birthdate > astronaut_analysis.ASTRONAUT_DATA_RETRIEVAL_DATE
    assert astronaut_analysis.calculate_age(birthdate) is None


def test_cleanup_data():
    # Read and cleanup data set
    astronauts_df = pd.read_json("../data/astronauts.json")
    astronauts_df = astronaut_analysis.cleanup_data_set(astronauts_df)

    # Make sure that age calculation produces values >= 0 or is NaN
    assert (astronauts_df["age"] >= 0).all()
    assert (astronauts_df.dropna(subset=["date_of_death"])["died_with_age"] >= 0).all()

    # Make sure that time in space has a valid range
    assert (astronauts_df["time_in_space_D"] >= 0).all()
